﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DesktopApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnObjektiISobe_Click(object sender, EventArgs e)
        {
            FormSobe formaObjekti = new FormSobe();
            formaObjekti.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnNova_Click(object sender, EventArgs e)
        {
            
        }

        private void btnRezervacije_Click(object sender, EventArgs e)
        {
            FormNovaRezervacija formNovaRezervacija = new FormNovaRezervacija();
            formNovaRezervacija.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ObjektiForm objektiForm = new ObjektiForm();
            objektiForm.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            GostiForm gostiForm = new GostiForm();
            gostiForm.ShowDialog();
        }
    }
}
