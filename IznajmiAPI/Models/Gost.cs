﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IznajmiAPI.Models
{
    public class Gost
    {
        public int? Id_gost { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }

        public Gost(int? id_gost, string ime, string prezime)
        {
            this.Id_gost = id_gost;
            this.Ime = ime;
            this.Prezime = prezime;
        }
    }
}
