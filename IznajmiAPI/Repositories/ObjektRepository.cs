﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IznajmiAPI.DbModels;
using Microsoft.EntityFrameworkCore;

namespace IznajmiAPI.Repositories
{
    public class ObjektRepository
    {
        private readonly dbRezervacijeContext _dbContext;

        public ObjektRepository(dbRezervacijeContext c)
        {
            _dbContext = c;
        }

        public IEnumerable<Models.Objekt> ObjektiIzBazeISobe()
        {
            return _dbContext.Objekts.Select(x => Mapper.ObjektMapper.FromDatabase(x));
        }

        public void SpremiObjekt(Models.Objekt objekt)
        {
            var dbObjekt = Mapper.ObjektMapper.ToDatabase(objekt);
            _dbContext.Objekts.Add(dbObjekt);
            _dbContext.SaveChanges();
        }

        public void BrisiObjekt(int id)
        {
            var obj = _dbContext.Objekts.Where(x => x.IdObjekt == id).Single();

            //izbrisat sve sobe
            List<DbModels.Soba> sobeIzBaze = new List<Soba>();
            foreach (var s in _dbContext.Sobas)
            {
                if(s.IdObjekt == id)
                {
                    //_dbContext.Sobas.Remove(s);
                    sobeIzBaze.Add(s);
                }
            }

            //izbrisat sve rezervacije za te sobe
            foreach (var r in _dbContext.Rezervacijas)
            {
                foreach (var soba in sobeIzBaze)
                {
                    if(r.IdSoba == soba.IdSoba)
                    {
                        _dbContext.Rezervacijas.Remove(r);
                    }
                }
            }

            foreach (var item in sobeIzBaze)
            {
                _dbContext.Sobas.Remove(item);
            }

            _dbContext.Objekts.Remove(obj);
            _dbContext.SaveChanges();
        }

        public void PromijeniNazivObjekta(Models.Objekt objekt)
        {
            var dbObjekt = Mapper.ObjektMapper.ToDatabase(objekt);
            _dbContext.Objekts.Update(dbObjekt);
            _dbContext.SaveChanges();
        }
    }
}