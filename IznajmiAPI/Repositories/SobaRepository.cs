﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IznajmiAPI.DbModels;
using Microsoft.EntityFrameworkCore;

namespace IznajmiAPI.Repositories
{
    public class SobaRepository
    {
        private readonly dbRezervacijeContext _context;

        public SobaRepository(dbRezervacijeContext c)
        {
            _context = c;
        }

        public List<Models.Soba> DohvatiSveSobeIObjekte()
        {
            return _context.Sobas.Include(s => s.IdObjektNavigation).Select(x => Mapper.SobaMapper.FromDatabase(x)).ToList();
        }

        public void BrisanjeSobe(int id)
        {
            var soba = _context.Sobas.Where(x => x.IdSoba == id).Single();
            //izbrisat i povezane rezervacije na tu sobu

            foreach (var rezervacija in _context.Rezervacijas)
            {
                if(rezervacija.IdSoba == id)
                {
                    _context.Rezervacijas.Remove(rezervacija);
                }
            }

            _context.Sobas.Remove(soba);
            _context.SaveChanges();
        }

        public void DodavanjeSobe(DbModels.Soba soba)
        {
            _context.Sobas.Add(soba);
            _context.SaveChanges();
        }

        public void AzuriranjePodatakaSobe(Models.Soba s)
        {
            //DbModels.Soba dbSoba = Mapper.SobaMapper.ToDatabase(s);
            var  dbSoba = _context.Sobas.Where(x => x.IdSoba == s.Id_soba).Single();
            if(s.Id_objekt_foreignKey == null)
            {
                dbSoba.Broj = s.BrojSobe;
            }
            else
            {
                //mijenja se i broj sobe i naziv
                dbSoba.Broj = s.BrojSobe;
                dbSoba.IdObjekt =(int) s.Id_objekt_foreignKey;
            }
            _context.SaveChanges();
        }
    }
}