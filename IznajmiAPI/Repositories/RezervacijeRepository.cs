﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IznajmiAPI.DbModels;
using Microsoft.EntityFrameworkCore;
using IznajmiAPI.Models;

namespace IznajmiAPI.Repository
{
    public class RezervacijeRepository
    {
        private readonly dbRezervacijeContext _dbContext;

        public RezervacijeRepository(dbRezervacijeContext c)
        {
            _dbContext = c;
        }

        public List<Models.Rezervacija> GetAll()
        {
           var  result = _dbContext.Rezervacijas
                .Include(r => r.IdGostNavigation)
                .Include(s => s.IdSobaNavigation)
                .ThenInclude(o => o.IdObjektNavigation)
                .Select(gost => Mapper.RezervacijaMapper.FromDatabase(gost)).ToList();
            return result;
        }

        public void BrisanjeRezervacije(int id)
        {
            DbModels.Rezervacija rez = _dbContext.Rezervacijas.Where(r => r.IdRezervacija == id).Single();
            _dbContext.Rezervacijas.Remove(rez);
            _dbContext.SaveChanges();
        }

        public void DodavanjeRezervacije(DbModels.Rezervacija r)
        {
            _dbContext.Rezervacijas.Add(r);
            _dbContext.SaveChanges();
        }

        public void AzuriranjeRezervacije(DbModels.Rezervacija r)
        {
            _dbContext.Rezervacijas.Update(r);
            _dbContext.SaveChanges();
        }
    }
}
