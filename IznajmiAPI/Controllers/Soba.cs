﻿using IznajmiAPI.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IznajmiAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Soba : ControllerBase
    {
        private SobaService _sobaService;

        public Soba(SobaService s)
        {
            _sobaService = s;
        }

        [HttpGet]
        public ActionResult<List<Models.Soba>> Get()
        {
            return _sobaService.DohvatiSobe();
        }

        [HttpDelete("{id}")]
        public void IzbrisiSobu(string id)
        {
            _sobaService.BrisanjeSobe(int.Parse(id));
        }

        [HttpPost]
        public void DodajSobu([FromBody] JObject json)
        {
            var soba = DtoMappers.SobaDto.FromJson(json);
            _sobaService.DodajSobu(Mapper.SobaMapper.ToDatabase(soba));
        }

        [HttpPut]
        public void Azuriraj([FromBody] JObject json)
        {
            var podaci = DtoMappers.SobaDto.PutMethodFromJson(json);
            _sobaService.AzurirajPodatkeSobe(podaci);
            //novi broj sobe, objekt kojem pripada, koja se soba minja
        }
    }
}
