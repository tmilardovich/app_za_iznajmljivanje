﻿using IznajmiAPI.Services;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IznajmiAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class Objekt : Controller
    {
        private ObjektService _objektService;

        public Objekt(ObjektService objektService)
        {
            _objektService = objektService;
        }

        [HttpGet]
        public ActionResult<List<Models.Objekt>> Get()
        {
            return _objektService.ObjektiISObe().ToList();
        }

        [HttpPost]
        public void SpremiObjekt([FromBody] JObject json)
        {
            var obj = DtoMappers.ObjektDto.FromJson(json);
            _objektService.SpremanjeObjekta(obj);
        }

        [HttpDelete("{id}")]
        public void IzbrisiObjekt(string id)
        {
            _objektService.BrisanjeObjekta(int.Parse(id));
        }

        [HttpPut]
        public void PromijeniNaziv([FromBody] JObject json)
        {
            var obj = DtoMappers.ObjektDto.FromJson(json);
            _objektService.PromjenaNaziva(obj);
        }

    }
}