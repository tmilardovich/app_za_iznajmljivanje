﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IznajmiAPI.Services;
using IznajmiAPI.Models;
using IznajmiAPI.DbModels;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;

namespace IznajmiAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class Rezervacije : Controller
    {
        private RezervacijeService _rezervacijeService;

        public Rezervacije(RezervacijeService rs)
        {
            _rezervacijeService = rs;
        }

        [HttpGet]
        public ActionResult<List<Models.Rezervacija>> Get()
        {
            return _rezervacijeService.GetAll();
        }

        [HttpPost]
        public void Add([FromBody] JObject json)
        {
            var rez = DtoMappers.RezervacijaDto.RezervacijaJsonToDatabase(json);
            _rezervacijeService.SpremanjeRezervacije(rez);
        }

        [HttpPut]
        public void Azuriranje([FromBody] JObject json)
        {
            var rez = DtoMappers.RezervacijaDto.RezervacijaJsonToDatabase(json);
            _rezervacijeService.Azuriranje(rez);
        }

        [HttpDelete("{id}")]
        public void Brisanje(string id)
        {
            _rezervacijeService.Brisanje(int.Parse(id));
        }
    }
}