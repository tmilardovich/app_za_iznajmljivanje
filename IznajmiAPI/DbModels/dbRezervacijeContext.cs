﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace IznajmiAPI.DbModels
{
    public partial class dbRezervacijeContext : DbContext
    {
        public dbRezervacijeContext()
        {
        }

        public dbRezervacijeContext(DbContextOptions<dbRezervacijeContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Gost> Gosts { get; set; }
        public virtual DbSet<Objekt> Objekts { get; set; }
        public virtual DbSet<Rezervacija> Rezervacijas { get; set; }
        public virtual DbSet<Soba> Sobas { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=DESKTOP-VNGMRPA;Database=dbRezervacije;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Croatian_CI_AS");

            modelBuilder.Entity<Gost>(entity =>
            {
                entity.HasKey(e => e.IdGost);

                entity.ToTable("GOST");

                entity.Property(e => e.IdGost).HasColumnName("ID_gost");

                entity.Property(e => e.Ime)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Prezime)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Objekt>(entity =>
            {
                entity.HasKey(e => e.IdObjekt);

                entity.ToTable("OBJEKT");

                entity.Property(e => e.IdObjekt).HasColumnName("ID_objekt");

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Rezervacija>(entity =>
            {
                entity.HasKey(e => e.IdRezervacija);

                entity.ToTable("REZERVACIJA");

                entity.Property(e => e.IdRezervacija).HasColumnName("ID_rezervacija");

                entity.Property(e => e.DatumDo).HasColumnType("date");

                entity.Property(e => e.DatumOd).HasColumnType("date");

                entity.Property(e => e.IdGost).HasColumnName("ID_gost");

                entity.Property(e => e.IdSoba).HasColumnName("ID_soba");

                entity.HasOne(d => d.IdGostNavigation)
                    .WithMany(p => p.Rezervacijas)
                    .HasForeignKey(d => d.IdGost)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_REZERVACIJA_GOST");

                entity.HasOne(d => d.IdSobaNavigation)
                    .WithMany(p => p.Rezervacijas)
                    .HasForeignKey(d => d.IdSoba)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_REZERVACIJA_SOBA");
            });

            modelBuilder.Entity<Soba>(entity =>
            {
                entity.HasKey(e => e.IdSoba);

                entity.ToTable("SOBA");

                entity.Property(e => e.IdSoba).HasColumnName("ID_soba");

                entity.Property(e => e.IdObjekt).HasColumnName("ID_objekt");

                entity.HasOne(d => d.IdObjektNavigation)
                    .WithMany(p => p.Sobas)
                    .HasForeignKey(d => d.IdObjekt)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SOBA_OBJEKT");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
