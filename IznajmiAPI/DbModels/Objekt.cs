﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IznajmiAPI.DbModels
{
    public partial class Objekt
    {
        public Objekt()
        {
            Sobas = new HashSet<Soba>();
        }

        public int IdObjekt { get; set; }
        public string Naziv { get; set; }

        public virtual ICollection<Soba> Sobas { get; set; }
    }
}
