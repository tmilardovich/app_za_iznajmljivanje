﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IznajmiAPI.DbModels
{
    public partial class Rezervacija
    {
        public int IdRezervacija { get; set; }
        public int IdGost { get; set; }
        public int IdSoba { get; set; }
        public DateTime DatumOd { get; set; }
        public DateTime DatumDo { get; set; }

        public virtual Gost IdGostNavigation { get; set; }
        public virtual Soba IdSobaNavigation { get; set; }
    }
}
