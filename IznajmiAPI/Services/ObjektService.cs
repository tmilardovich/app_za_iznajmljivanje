﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IznajmiAPI.Repositories;
namespace IznajmiAPI.Services
{
    public class ObjektService
    {
        private ObjektRepository _objektRepository;

        public ObjektService(ObjektRepository or)
        {
            _objektRepository = or;
        }

        public IEnumerable<Models.Objekt> ObjektiISObe()
        {
            return _objektRepository.ObjektiIzBazeISobe();
        }

        public void SpremanjeObjekta(Models.Objekt o)
        {
            _objektRepository.SpremiObjekt(o);
        }
        
        public void BrisanjeObjekta(int id)
        {
            _objektRepository.BrisiObjekt(id);
        }

        public void PromjenaNaziva(Models.Objekt objekt)
        {
            _objektRepository.PromijeniNazivObjekta(objekt);
        }
    }
}
