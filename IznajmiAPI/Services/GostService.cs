﻿using IznajmiAPI.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IznajmiAPI.Models;

namespace IznajmiAPI.Services
{
    public class GostService
    {
        private GostRepository _gostRepository;

        public GostService(GostRepository gostRepository)
        {
            _gostRepository = gostRepository;
        }

        public IEnumerable<Gost> DohvatiGosteService()
        {
            return _gostRepository.DohvatiGoste();
        }

        public void DodavanjeGosta(Models.Gost g)
        {
            _gostRepository.DodajGosta(Mapper.GostMapper.ToDatabase(g));
        }

        public void Brisanje(int id)
        {
            _gostRepository.BrisanjeGosta(id);
        }

        public void Azuriranje(Models.Gost g)
        {
            _gostRepository.Azuriranje(Mapper.GostMapper.ToDatabase(g));
        }
    }
}