﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IznajmiAPI.Mapper
{
    public class ObjektMapper
    {
        public static Models.Objekt FromDatabase(DbModels.Objekt objekt)
        {
            return new Models.Objekt(objekt.IdObjekt, objekt.Naziv);
        }

        public static DbModels.Objekt ToDatabase(Models.Objekt o)
        {
            DbModels.Objekt objekt = new DbModels.Objekt();
            objekt.IdObjekt = (int)o.Id_objekt;
            objekt.Naziv = o.Naziv;
            return objekt;
        }
    }
}
