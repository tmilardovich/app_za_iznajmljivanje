﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IznajmiAPI.Mapper
{
    public class SobaMapper
    {
        public static Models.Soba FromDatabase(DbModels.Soba s)
        {
            return new Models.Soba(s.IdSoba, s.Broj, s.IdObjekt, Mapper.ObjektMapper.FromDatabase(s.IdObjektNavigation));
        }

        public static DbModels.Soba ToDatabase(Models.Soba s)
        {
            DbModels.Soba dbSoba = new DbModels.Soba();
            dbSoba.IdSoba = 0;
            dbSoba.Broj = s.BrojSobe;
            dbSoba.IdObjekt = (int)s.Id_objekt_foreignKey;
            return dbSoba;
        }
    }
}