﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IznajmiAPI.Mapper
{
    public static class RezervacijaMapper
    {
        public static Models.Rezervacija FromDatabase(DbModels.Rezervacija r)
        {
            return new Models.Rezervacija(r.IdRezervacija, r.IdGost, r.IdSoba, r.DatumOd, r.DatumDo, 
                Mapper.GostMapper.FromDatabase(r.IdGostNavigation), 
                Mapper.SobaMapper.FromDatabase(r.IdSobaNavigation)
                );
        }
    }
}
