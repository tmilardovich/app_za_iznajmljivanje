﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IznajmiAPI.Mapper
{
    public class GostMapper
    {
        public static Models.Gost FromDatabase(DbModels.Gost gost)
        {
            return new Models.Gost(gost.IdGost, gost.Ime, gost.Prezime);
        }

        public static DbModels.Gost ToDatabase(Models.Gost g)
        {
            DbModels.Gost dbGost = new DbModels.Gost();
            dbGost.IdGost = (int)g.Id_gost;
            dbGost.Ime = g.Ime;
            dbGost.Prezime = g.Prezime;
            return dbGost;
        }
    }
}
