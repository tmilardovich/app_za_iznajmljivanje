﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IznajmiAPI.DtoMappers
{
    public static class GostDto
    {
        public static Models.Gost FromJson(JObject json)
        {
            var id = json["id"].ToObject<int?>(); ///////////////////////////////////////////////////////////////////////////
            var ime = json["ime"].ToObject<string>();
            var prezime = json["prezime"].ToObject<string>();

            if(id == null)
            {
                return new Models.Gost(0, ime, prezime);
            }
            else
            {
                return new Models.Gost(id, ime, prezime);
            }
        }
    }
}