﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IznajmiAPI.DtoMappers
{
    public static class ObjektDto
    {
        public static Models.Objekt FromJson(JObject json)
        {
            var naziv = json["naziv"].ToObject<string>();
            var objektid = json["id_objekt"].ToObject<int?>();

            return new Models.Objekt(objektid,naziv);
        }
    }
}
