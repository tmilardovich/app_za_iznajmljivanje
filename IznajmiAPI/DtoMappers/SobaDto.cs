﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IznajmiAPI.DtoMappers
{
    public static class SobaDto
    {
        public static Models.Soba FromJson(JObject json){
            var brojSobe = json["broj_sobe"].ToObject<int>();
            var objekt = json["objekt_id"].ToObject<int>();

            int? idSobe = json["soba_id"].ToObject<int?>();
            
            if (idSobe == 0 )
            {
                return new Models.Soba(0, brojSobe, objekt, new Models.Objekt(0, ""));
            }
            else {
                return new Models.Soba(idSobe, brojSobe, objekt, new Models.Objekt(0, ""));
            }

            
        }

        public static Models.Soba PutMethodFromJson(JObject json)
        {
            var brojSobe = json["broj_sobe"].ToObject<int>();
            var objekt = json["objekt_id"].ToObject<int?>();
            var idSobe = json["soba_id"].ToObject<int?>();

            return new Models.Soba(idSobe, brojSobe, objekt, new Models.Objekt(0, ""));
        }
    }
}